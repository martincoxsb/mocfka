-module(mocfka_node_server).
-behaviour(gen_server).

%% API.
-export([start_link/0]).

%% gen_server.
-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).

-record(state, {
          intercept_mods = []
}).

-include_lib("kernel/include/logger.hrl").

-define(INTERVAL, 1000).
-define(ERLANG_LS_NODE_PREFIX, "erlang_ls").

%% API.

-spec start_link() -> {ok, pid()}.
start_link() ->
    gen_server:start_link({global, ?MODULE}, ?MODULE, [], []).

%% gen_server.

init([]) ->
    ok = net_kernel:monitor_nodes(true, [{node_type, all}]),
    check_epmd(),
    {ok, #state{intercept_mods = application:get_env(mocfka, intercepts, [])}}.

handle_call({M, F, A}, _From, State) ->
    Result = erlang:apply(M, F, A),
    {reply, Result, State};

handle_call(_Request, _From, State) ->
    {reply, ignored, State}.

handle_cast({M, F, A}, State) ->
    erlang:apply(M, F, A),
    {noreply, State};

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(check_epmd, State) ->
    check_epmd(),
    {noreply, State};

handle_info({nodeup, Node, _Info}, State) ->
    ?LOG_INFO(#{event => nodeup, node => Node}),
    rpc:call(Node, code, add_path, [code:lib_dir(mocfka, ebin)]),
    create_intercepts(Node, State#state.intercept_mods),
    {noreply, State};

handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

check_epmd() ->
    lists:foreach(fun(Node) -> net_adm:ping(Node) end, node_names()),
    erlang:send_after(?INTERVAL, self(), check_epmd).

node_names() ->
    {ok, Names} = net_adm:names(),
    [list_to_atom(Name ++ "@127.0.0.1") || {Name, _Port} <- Names, filter_node(Name)] -- nodes().

filter_node(Name) ->
    case string:find(Name, ?ERLANG_LS_NODE_PREFIX) of
        nomatch -> true;
        _ -> false
    end.

create_intercepts(Node, InterceptMods) ->
    lists:foreach(fun(InterceptMod) -> 
                          create_intercept(Node, InterceptMod:intercept(Node), InterceptMod) 
                  end, InterceptMods).

create_intercept(_Node, [], _InterceptMod) -> ok;
create_intercept(Node, [#{module := M, function := F, arity := A}|T], InterceptMod) ->
    rpc:call(Node, meck, expect, [M, F, fun InterceptMod:callback/A]),
    create_intercept(Node, T, InterceptMod).
