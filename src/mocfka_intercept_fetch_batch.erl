-module(mocfka_intercept_fetch_batch).

-export([intercept/1,
         callback/6,
         fetch_batch/6]).

-include_lib("kernel/include/logger.hrl").

intercept(Node) ->
    Calls = mocfka_util:find_calls(Node, '".*":"fetch_batch"/"(.*)"'),
    [#{module => Mod, 
       function => Fun, 
       arity => Arity} || {Mod, Fun, Arity} <- Calls].

callback(C, G, T, P, B, M) ->
    Batch = gen_server:call({global, mocfka_node_server},
                            {mocfka_intercept_fetch_batch, fetch_batch, [C, G, T, P, B, M]}),
    meck:passthrough([C, G, T, P, B ++ Batch, M]).

fetch_batch(C, G, T, P, B, M) ->
    ?LOG_INFO(#{cluster => C,
                group => G,
                topic => T,
                partition => P,
                batch => B,
                messages => M}),
    kafire_message:decode(mocfka_ets:fetch(G, T, P)).
