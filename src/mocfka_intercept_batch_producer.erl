-module(mocfka_intercept_batch_producer).

-export([intercept/1,
         callback/5,
         produce/5]).

-include_lib("kernel/include/logger.hrl").

intercept(_Node) ->
    [#{module => kafire_batch_producer, 
       function => produce,
       arity => 5}].

callback(C, T, P, O, B) ->
    gen_server:cast({global, mocfka_node_server},
                    {mocfka_intercept_batch_producer, produce, [C, T, P, O, B]}).

produce(C, T, P, O, B) ->
    ?LOG_INFO(#{cluster => C,
                topic => T,
                partition => P,
                opts => O,
                batch => B}),
    mocfka_ets:put_or_create(T, P, B).
