-module(mocfka_ets).
-behaviour(gen_server).

%% API.
-export([start_link/0]).
-export([put_or_create/3]).
-export([fetch/3]).

%% gen_server.
-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).

%% API.

-spec start_link() -> {ok, pid()}.
start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

put_or_create(T, P, B) ->
    gen_server:call(?MODULE, {put_or_create, T, P, B}).

fetch(G, T, P) ->
    gen_server:call(?MODULE, {fetch, G, T, P}).

%% gen_server.

init([]) ->
    {ok, maps:new()}.

handle_call({put_or_create, T, P, B}, _From, State) when is_map_key({T, P}, State) ->
    Next = next_pos(table_name(T, P)),
    true = ets:insert(maps:get({T, P}, State), {Next, P, B}),
    {reply, ok, State};

handle_call({put_or_create, T, P, B}, _From, State) ->
    Tid = ets:new(table_name(T, P), [named_table, ordered_set]),
    true = ets:insert(Tid, {next_pos(Tid), P, B}),
    {reply, ok, State#{{T, P} => Tid}};

handle_call({fetch, G, T, P}, _From, #{groups := Groups} = State) when is_map_key(G, Groups) ->
    Next = maps:get(G, Groups) + 1,
    Batch = ets:lookup(table_name(T, P), Next),
    {reply, Batch, State#{groups => Groups#{G => Next}}};

handle_call({fetch, G, T, P}, _From, #{groups := Groups} = State) ->
    Batch = ets:lookup(table_name(T, P), 1),
    {reply, Batch, State#{groups => Groups#{G => 1}}};

handle_call(_Request, _From, State) ->
    {reply, [], State}.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

table_name(T, P) ->
    PBin = integer_to_binary(P),
    binary_to_atom(<<T/binary, "_", PBin/binary>>).

next_pos('$end_of_table') -> 1;
next_pos(N) when is_integer(N) -> N + 1;
next_pos(Tid) ->
    next_pos(ets:last(Tid)).
