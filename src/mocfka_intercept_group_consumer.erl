-module(mocfka_intercept_group_consumer).

-export([intercept/1,
         callback/3,
         offset_commit/3]).

-include_lib("kernel/include/logger.hrl").

intercept(_Node) ->
    [#{module => kafire_group_consumer, 
       function => offset_commit,
       arity => 3}].

callback(C, G, T) ->
    gen_server:cast({global, mocfka_node_server},
                    {mocfka_intercept_group_consumer, offset_commit, [C, G, T]}).

offset_commit(C, G, T) ->
    ?LOG_INFO(#{cluster => C,
                group => G,
                topic => T}).
