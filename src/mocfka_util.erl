-module(mocfka_util).

-export([find_calls/2]).

find_calls(Node, XCall) ->
    _ = rpc:call(Node, xref, start, [s]),
    Path = rpc:call(Node, code, lib_dir, 
                    [list_to_atom(string:sub_word(atom_to_list(Node), 1, $@)), ebin]),
    _ = rpc:call(Node, xref, add_directory, [s, Path]),
    {ok, Calls} = rpc:call(Node, xref, q, [s, XCall]),
    Calls.
