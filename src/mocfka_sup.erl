-module(mocfka_sup).
-behaviour(supervisor).

-export([start_link/0]).
-export([init/1]).

start_link() ->
	supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init([]) ->
	Procs = [#{id => mocfka_node_server,
               start => {mocfka_node_server, start_link, []},
               restart => permanent,
               shutdown => 5000},

	         #{id => mocfka_ets,
               start => {mocfka_ets, start_link, []},
               restart => permanent,
               shutdown => 5000}
            ],

	{ok, {{one_for_one, 1, 5}, Procs}}.
