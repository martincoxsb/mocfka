#-*- mode: makefile-gmake -*-

PROJECT = mocfka
PROJECT_DESCRIPTION = Intercept Kafka calls whilst developing locally
PROJECT_VERSION := ${shell git describe --tags --always}
RELX_TAR = 0

DEPS = meck \
	   kafire

dep_kafire = git git@gitlab.com:superbet/kafire.git

dep_kafire_commit = 1.3.0

REL_DEPS = relx

SHELL_DEPS = \
	sync

SHELL_OPTS = \
	-config dev.config \
	-s $(PROJECT) \
	-s sync \
	+pc unicode

include erlang.mk
